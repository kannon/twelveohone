package source.Objects 
{
	import net.flashpunk.Entity;
	import source.Assets;
	import net.flashpunk.graphics.Image;
	
	/**
	 * ...
	 * @author ...
	 */
	public class Slime extends Entity 
	{
		
		public function Slime(x:int, y:int) 
		{
			//set position
			super(x, y);
			
			//set graphic and mask
			graphic = new Image(Assets.OBJECT_SLIME);
			setHitbox(32, 32);
			
			//set type
			type = "Slime";
		}
		
		
	}

}