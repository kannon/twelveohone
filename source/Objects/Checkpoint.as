package source.Objects 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import source.Assets;
	
	/**
	 * ...
	 * @author ...
	 */
	public class Checkpoint extends Entity 
	{
		
		public function Checkpoint(x:int, y:int) 
		{
			//set position
			super(x, y);
			
			//set graphic and mask
			graphic = new Image(Assets.OBJECT_CHECKPOINT);
			
			setHitbox(32, 32);
			
			//set type
			type = "Checkpoint";
		}
		
	}

}