package source.Objects
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	import source.Assets;
	import source.Objects.Enemies.EnemyBase;
	
	/**
	 * ...
	 * @author 
	 */
	public class BasicShot extends Entity 
	{
		private var direction:Boolean;
		private var speed:int = 400;
		
		public function BasicShot(playerFacing:Boolean) 
		{
			
			setHitbox(10, 10);
			type = "playerProjectile";
			
			//current player direction (true = right, false = left)
			direction = playerFacing;
			if (playerFacing == true)
			{
				graphic = new Image(Assets.PROJECTILE_BULLET_RIGHT);
			}
			else
			{
				graphic = new Image(Assets.PROJECTILE_BULLET_LEFT);
			}
		}
		
		override public function update():void
		{
			if (direction == true)
			{
				x += speed * FP.elapsed;
			}
			
			if (direction == false)
			{
				x -= speed* FP.elapsed;
			}
			
			if (collide("Solid", x, y))
			{
				destroy();
			}
			
			
			var e:EnemyBase = collide("Enemy", x, y) as EnemyBase;
			
			if (e)
			{
				
			}
			
			var s:Switch = collide("Switch", x, y) as Switch;
			if (s)
			{
				s.toggleSwitch();
				this.destroy();
			}
		}
		
		public function destroy():void 
		{
			
			FP.world.remove(this);
		}
		
	}

}