package source.Objects 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import source.Global;
	import source.Assets;
	/**
	 * ...
	 * @author Noel Berry
	 */
	public class Moving extends Physics
	{
		public var sprite:Image = new Image(Assets.OBJECT_MOVING);
		
		//True is right, false, left.
		public var direction:Boolean = FP.choose(true, false);
		public var movement:Number = 2;
		public var oneWay:Boolean = false;
		public var switchName:String;
		public var carry:Array = new Array("Solid", "Player");
		
		public function Moving(x:int, y:int, s:String="", o:String="") 
		{
			//our x/y position
			super(x, y);
			//graphic & hitbox
			graphic = sprite;
			switchName = s;
			if (o == "true")
			{
				oneWay = true
			}
			if (o == "false")
			{
				oneWay = false
			}
			setHitbox(64, 16);
			setOrigin(0,-16)
		}
		
		override public function update():void {
			
			//move in the correct direction
			speed.x = direction ? movement : - movement;
			
			//move stuff that's on top of us, for each type of entity we can carry
			for each(var i:String in carry) {
				moveontop(i,speed.x);
			}
			
			//move ourselves
			motion();
			
			//if we've stopped moving, switch directions!
			if (!oneWay)
			{
				if ( speed.x == 0 ) { direction = !direction; }
			}
		
		
			if (switchName != "")
			{
				oneWay = !Global.switchDict[switchName];
			}
		
		}
		public function flipDirection():void
		{
			direction = !direction
		}
		
	}

}