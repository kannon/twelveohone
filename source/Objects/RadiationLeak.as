package source.Objects 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import source.Assets;
	
	/**
	 * ...
	 * @author ...
	 */
	public class RadiationLeak extends Entity 
	{
		
		public function RadiationLeak(x:int, y:int) 
		{
			//set position
			super(x, y);
			
			//set graphic and mask
			graphic = new Image(Assets.OBJECT_RADIATIONLEAK);
			setHitbox(32, 32);
			
			//set type
			type = "Radiation";
		}
		
	}

}