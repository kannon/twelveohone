package source.Objects 
{

	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import source.Assets;
	import source.Global;
	/**
	 * ...
	 * @author
	 */
	public class VerticalPlatform extends Physics
	{
		public var sprite:Image = new Image(Assets.OBJECT_MOVINGVERTICAL);
		
		//False is up, true is down.
		public var direction:Boolean;
		public var oneWay:Boolean = false;
		public var movement:Number = 2;
		public var carry:Array = new Array("Solid", "Player");
		public var switchName:String = "";
		
		public function VerticalPlatform(x:int, y:int, s:String="", o:String="", d:String="") 
		{
			//our x/y position
			super(x, y);
			//graphic & hitbox
			graphic = sprite;
			setHitbox(32, 64);
			switchName = s;
			trace(direction);
			if (d == "true")
			{
				direction = true
			}
			if (d == "false")
			{
				direction = false
			}
			
			if (o == "true")
			{
				oneWay = true
			}
			if (o == "false")
			{
				oneWay = false
			}
			setOrigin(0,0)
		}
		
		override public function update():void {
			
			//move in the correct direction
			speed.y = direction ? movement : - movement;
			
			//move stuff that's on top of us, for each type of entity we can carry
			for each(var i:String in carry) {
				moveontop(i,speed.x);
			}
			
			//move ourselves
			motion();
			
			//if we've stopped moving, switch directions!
			if (!oneWay)
			{
				if ( speed.y == 0 ) { direction = !direction; }
			}
			
			if (switchName != "")
			{
				direction = !Global.switchDict[switchName];
			}
		}
		
		public function flipDirection():void
		{
			direction = !direction
		}
	}

}