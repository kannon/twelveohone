package source.Objects.Powerups 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import source.Assets;
	import source.Objects.Player;
	import net.flashpunk.graphics.Image;
	import source.Global;
	
	/**
	 * ...
	 * @author ...
	 */
	public class GunPowerup extends Entity
	{
		
		public function GunPowerup(x:int, y:int) 
		{
			super(x, y);
			graphic = new Image(Assets.POWERUP_GUN);
			setHitbox(32,32)
			type = "Powerup";
		}
		
		override public function update():void
		{
			var p:Player = collide("Player", x, y) as Player;
			if (p)
			{
				Global.hasGun = true;
				this.destroy();
			}
		}
		
		public function destroy():void 
		{
			
			FP.world.remove(this);
		}
		
	}

}