package source.Objects.Enemies 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import source.Assets;
	
	/**
	 * ...
	 * @author 
	 */
	public class EnemyBase extends Entity 
	{
		public var healthMax:Number = 15;
		public var healthCurrent:Number = 15;
		
		public function EnemyBase() 
		{
			graphic = new Image();
			setHitbox(10, 10);
			type = "Enemy";
			
			
		}
		
	}

}