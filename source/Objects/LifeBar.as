package source.Objects 
{
    import net.flashpunk.Entity;
	import source.Global;
    import net.flashpunk.FP;
    import net.flashpunk.graphics.Image;
    import net.flashpunk.utils.Input;
    import net.flashpunk.utils.Key;
    
    public class LifeBar extends Entity
    {
        public var bar:Image = Image.createRect(100, 10, 0xFF0000);
        public var lastHealth:int = 100;
		private var diff:int;
		
        public function LifeBar() 
        {
            super(200, 0, bar);
        }
        
        override public function update():void 
        {
			if (lastHealth >= Global.player.healthCurrent)
			{
				diff = lastHealth - Global.player.healthCurrent;
                bar.clipRect.width = FP.approach(bar.clipRect.width, 0, diff);
                bar.clear();
                bar.updateBuffer();
				lastHealth = Global.player.healthCurrent;
			}
			
			if (lastHealth <= Global.player.healthCurrent)
			{
				diff = Global.player.healthCurrent - lastHealth;
                bar.clipRect.width = FP.approach(bar.clipRect.width, bar.width, diff);
                bar.clear();
                bar.updateBuffer();
				lastHealth = Global.player.healthCurrent;
			}
        }
    }
}