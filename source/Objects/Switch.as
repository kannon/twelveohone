package source.Objects 
{
	import flash.geom.Rectangle;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.utils.Input;
	import source.Assets;
	import source.Control.Game;
	import source.Global;
	
	/**
	 * ...
	 * @author ...
	 */
	public class Switch extends Entity 
	{
		public var sprite_inactive:Image = new Image(Assets.OBJECT_SWITCH, new Rectangle(0, 0, 32, 64));
		public var sprite_active:Image = new Image(Assets.OBJECT_SWITCH, new Rectangle(32, 0, 32, 64));
		public var state:Boolean;
		public var name:String;
		
		public function Switch(x:int, y:int, n:String, s:String="false") 
		{
			super(x, y);
			
			graphic = sprite_inactive;
			setHitbox(32,32)
			type = "Switch";
			name = n;
			if (s == "true")
			{
				state = true
			}
			if (s == "false")
			{
				state = false
			}
			Global.switchDict[n.toString()] = state;
			trace(Global.switchDict[n.toString()]);
		}
		
		override public function update():void
		{
			
		}
		
		public function toggleSwitch():void
		{
			if (!state)
			{
				graphic = sprite_active;
			}
			if (state)
			{
				graphic = sprite_inactive
			}
			state = !state;
			Global.switchDict[name.toString()] = state;
			trace(Global.switchDict[name.toString()]);
		}
		
	}

}