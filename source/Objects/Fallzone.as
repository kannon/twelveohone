package source.Objects 
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	import source.Assets;
	
	/**
	 * ...
	 * @author ...
	 */
	public class Fallzone extends Entity 
	{
		
		public function Fallzone(x:int, y:int) 
		{
			//set position
			super(x, y);
			
			//set graphic and mask
			graphic = new Image(Assets.OBJECT_FALLZONE);
			
			setHitbox(32, 32);
			
			//set type
			type = "Fallzone";
			
		}
		
	}

}