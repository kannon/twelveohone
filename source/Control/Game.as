package source.Control
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import source.Assets;
	import source.Global;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.World;
	import net.flashpunk.graphics.Tilemap;
	import org.flashdevelop.utils.FlashViewer;
	import source.Objects.Powerups.CodePowerup;
	import source.Objects.Powerups.GunPowerup;
	import source.Objects.Powerups.JumpPowerup;
	import source.Objects.Transitions.BunkerDoor;
	import source.Objects.Transitions.OfficeDoor;
	import source.Objects.Transitions.OverworldDoor;
	import source.Objects.Transitions.PoliceDoor;
	
	import source.Objects.*;
	import source.Solids.*;
	/**
	 * ...
	 * @author Noel Berry
	 */
	public class Game extends World
	{
		
		public var tileset:Tilemap;
		
		//timer so that reset level doesn't happen right away...
		public var reset:int = 60;
		
		override public function begin():void
		{
			//enable the console
			FP.console.enable();
			
			//set the level to 0
			Global.level = 0;
			Global.lastLevelIndex = 0;
			
			//load next level
			loadspecificlevel(0);
		}
		
		override public function update():void 
		{
			//only update if the game is not paused
			if (!Global.paused) { super.update(); }
			
			//if we should restart
			if (Global.restart) { 
				
				//make a timer so it isn't instant
				reset -= 1;
				if (reset == 0) {
					//restart level
					restartlevel();
					//set restart to false
					Global.restart = false;
					//reset our timer
					reset = 60;
				}
			}
			
			//if we should restart from a checkpoint.
			if (Global.restartFromCheckpoint) { 
				
				//make a timer so it isn't instant
				reset -= 1;
				if (reset == 0) {
					//restart level
					restartFromCheckpoint();
					//set restart to false
					Global.restartFromCheckpoint = false;
					//reset our timer
					reset = 60;
				}
			}
			
			//load next level on level completion
			if (Global.finished) {
				gotoLevel(Global.nextLevelIndex);
			}
			
		}
		/*
		public function loadlevel():void 
		{	
			//get the XML
			var file:ByteArray = new Assets.LEVELS[Global.level-1];
			var str:String = file.readUTFBytes( file.length );
			var xml:XML = new XML(str);
			
			//define some variables that we will use later on
			var e:Entity;
			var o:XML;
			var n:XML;
			
			//set the level size
			FP.width = xml.width;
			FP.height = xml.height;
			
			//add the tileset to the world
			add(new Entity(0, 0, tileset = new Tilemap(Assets.TILESET, FP.width, FP.height, Global.grid, Global.grid)));
			
			//add the view, and the player
			add(Global.player = new Player(xml.objects[0].player.@x, xml.objects[0].player.@y));
			
			//set the view to follow the player, within no restraints, and let it "stray" from the player a bit.
			//for example, if the last parameter was 1, the view would be static with the player. If it was 10, then
			//it would trail behind the player a bit. Higher the number, slower it follows.
			add(Global.view = new View(Global.player as Entity, new Rectangle(0,0,FP.width,FP.height), 10));
			
			//add tiles
			for each (o in xml.tilesAbove[0].tile) {
				//place the tiles in the correct position
				//NOTE that you should replace the "5" with the amount of columns in your tileset!
				tileset.setTile(o.@x / Global.grid, o.@y / Global.grid, (5 * (o.@ty/Global.grid)) + (o.@tx/Global.grid));
			}
			
			//place the solids
			for each (o in xml.floors[0].rect) {
				//place flat solids, setting their position & width/height
				add(new Solid(o.@x, o.@y, o.@w, o.@h));
			}
			if(str.search("<slopes>") > 0) {
				for each (o in xml.slopes[0].slope) {
					//place a slope
					add(new Slope(o.@x, o.@y, o.@type));
				}
			}
			//place a crate
			for each (o in xml.objects[0].crate) { add(new Crate(o.@x, o.@y)); }
			
			//place a moving platform
			for each (o in xml.objects[0].moving) { add(new Moving(o.@x, o.@y)); }
			
			//place a moving platform
			for each (o in xml.objects[0].spikes) { add(new Spikes(o.@x, o.@y)); }
			
			//Add slime!
			for each (o in xml.objects[0].slime) { add(new Slime(o.@x, o.@y)); }
			
			//Add radiation leaks.
			for each (o in xml.objects[0].radiation) { add(new RadiationLeak(o.@x, o.@y)); }
			
			//Add instakill fall zones.
			for each (o in xml.objects[0].fallzone) { add(new Fallzone(o.@x, o.@y)); }
			
			//place electricity
			for each (o in xml.objects[0].electricity) {
				var p:Point;
				//get the end point of the electricity (via nodes)
				for each (n in o.node)
				{
					p = new Point(n.@x, n.@y);
				}
				
				//add electricity to the world
				add(new Electricity(o.@x, o.@y, p.x, p.y));
			}
			
			//add the door!
			for each (o in xml.objects[0].door) { add(new Door(o.@x, o.@y)); }
		}
		*/
		
		//Yes, it's a hack. I'll refactor it later before I submit it back to APE.
		public function loadlevel():void
		{
			loadspecificlevel(Global.level-1)
		}
		
		public function loadspecificlevel(index:int):void 
		{	
			//get the XML
			var file:ByteArray = new Assets.LEVELS[index];
			var str:String = file.readUTFBytes( file.length );
			var xml:XML = new XML(str);
			
			//define some variables that we will use later on
			var e:Entity;
			var o:XML;
			var n:XML;
			
			//set the level size
			FP.width = xml.width;
			FP.height = xml.height;
			
			//add the tileset to the world
			add(new Entity(0, 0, tileset = new Tilemap(Assets.TILESET, FP.width, FP.height, Global.grid, Global.grid)));
			/*
			//add the view, and the player
			add(Global.player = new Player(xml.objects[0].player.@x, xml.objects[0].player.@y));
			Global.lastCheckpoint = [xml.objects[0].player.@x, xml.objects[0].player.@y]
			*/
			for each (o in xml.objects[0].PoliceDoor) { add(new PoliceDoor(o.@x, o.@y)); }
			for each (o in xml.objects[0].OverworldDoor) { add(new OverworldDoor(o.@x, o.@y)); }
			for each (o in xml.objects[0].BunkerDoor) { add(new BunkerDoor(o.@x, o.@y)); }
			for each (o in xml.objects[0].OfficeDoor) { add(new OfficeDoor(o.@x, o.@y)); }

			Global.lastLevelIndex = Global.level;
			Global.level = index;
			if (index == 1)
			{
				trace("Going to the overworld")
				if (Global.lastLevelIndex == 0)
				{
					//Spawn from the bunker door
					trace(Global.lastLevelIndex, "Changing to ", index);
					add(Global.player = new Player(xml.objects[0].BunkerDoor.@x, xml.objects[0].BunkerDoor.@y));
				}
				
				
				if (Global.lastLevelIndex == 2)
				{
					//Spawn from police office.
					trace(Global.lastLevelIndex, "Changing to ", index);
					add(Global.player = new Player(xml.objects[0].PoliceDoor.@x, xml.objects[0].PoliceDoor.@y));
				}
				
				if (Global.lastLevelIndex == 3)
				{
					//Spawn from office building
					trace(Global.lastLevelIndex, "Changing to ", index);
					add(Global.player = new Player(xml.objects[0].OfficeDoor.@x, xml.objects[0].OfficeDoor.@y));
				}
			}
			else
			{
				//add the player
				add(Global.player = new Player(xml.objects[0].player.@x, xml.objects[0].player.@y));
				Global.lastCheckpoint = [xml.objects[0].player.@x, xml.objects[0].player.@y]
			}
			
			add(Global.view = new View(Global.player as Entity, new Rectangle(0,0,FP.width,FP.height), 10));
			
			//set the view to follow the player, within no restraints, and let it "stray" from the player a bit.
			//for example, if the last parameter was 1, the view would be static with the player. If it was 10, then
			//it would trail behind the player a bit. Higher the number, slower it follows.
			
			
			//add tiles
			for each (o in xml.tilesAbove[0].tile) {
				//place the tiles in the correct position
				//NOTE that you should replace the "5" with the amount of columns in your tileset!
				tileset.setTile(o.@x / Global.grid, o.@y / Global.grid, (5 * (o.@ty/Global.grid)) + (o.@tx/Global.grid));
			}
			
			//place the solids
			for each (o in xml.floors[0].rect) {
				//place flat solids, setting their position & width/height
				add(new Solid(o.@x, o.@y, o.@w, o.@h));
			}
			if(str.search("<slopes>") > 0) {
				for each (o in xml.slopes[0].slope) {
					//place a slope
					add(new Slope(o.@x, o.@y, o.@type));
				}
			}
			//place a crate
			for each (o in xml.objects[0].crate) { add(new Crate(o.@x, o.@y)); }
			
			//place a moving platform
			for each (o in xml.objects[0].moving) { add(new Moving(o.@x, o.@y, o.@switchname, o.@oneway)); }
			for each (o in xml.objects[0].verticalmoving) { 
				/*
				if (o.@switchname && o.@oneway &&)
				{
					add(new VerticalPlatform(o.@x, o.@y, o.@switchname, o.@oneway, o.@direction));
				}
				if (o.@switchname)
				{
					add(new VerticalPlatform(o.@x, o.@y, o.@switchname));
				}
				*/
				add(new VerticalPlatform(o.@x, o.@y, o.@switchname, o.@oneway, o.@direction));
			}
			//place a moving platform
			for each (o in xml.objects[0].spikes) { add(new Spikes(o.@x, o.@y)); }
			
			//Add slime!
			for each (o in xml.objects[0].slime) { add(new Slime(o.@x, o.@y)); }
			
			//Add radiation leaks.
			for each (o in xml.objects[0].radiation) { add(new RadiationLeak(o.@x, o.@y)); }
			
			//Add instakill fall zones.
			for each (o in xml.objects[0].fallzone) { add(new Fallzone(o.@x, o.@y)); }
			
			//place electricity
			for each (o in xml.objects[0].electricity) {
				var p:Point;
				//get the end point of the electricity (via nodes)
				for each (n in o.node)
				{
					p = new Point(n.@x, n.@y);
				}
				
				//add electricity to the world
				add(new Electricity(o.@x, o.@y, p.x, p.y));
			}
			
			if (Global.hasGun == false) {
				for each (o in xml.objects[0].gunpowerup) { add(new GunPowerup(o.@x, o.@y)); }
			}
			if (Global.hasDoubleJump == false) {
				for each (o in xml.objects[0].jumppowerup) { add(new JumpPowerup(o.@x, o.@y)); }
			}
			
			if (Global.hasSecurityCodes == false) {
				for each (o in xml.objects[0].codepowerup) { add(new CodePowerup(o.@x, o.@y)); }
			}
			//Add transitions
			//Yes, I know this is a hack. Doing it properly needs to be able to set properties on the tiles.
			//Which isn't something current-ogmo does. 2.0 does, and so I'll implement it then...
			//Also, LD, so fast.
			

			
			//Add a checkpoint!
			for each (o in xml.objects[0].checkpoint) { add(new Checkpoint(o.@x, o.@y)); }
			
			//Add switches!
			for each (o in xml.objects[0].switchlocation) { add(new Switch(o.@x, o.@y, o.@name, o.@state)); trace("Switch added!"); }
			
			//add the door!
			for each (o in xml.objects[0].door) { add(new Door(o.@x, o.@y)); }
			
			//A hack for looking like you come through a door, until I can fix it properly, later.
			/**
			 * This checks if you're going to the overworld, which is index 1, and which index you came from (LastLevel)
			 * And places you appropriately.
			 */
			
			
			
			
			
			//set the view to follow the player, within no restraints, and let it "stray" from the player a bit.
			//for example, if the last parameter was 1, the view would be static with the player. If it was 10, then
			//it would trail behind the player a bit. Higher the number, slower it follows.
			
			
			
		}
		
		/**
		 * Loads up the next level (removes all entities of the current world, increases Global.level, calls loadlevel)
		 * @return	void
		 */
		public function nextlevel():void
		{
			removeAll();
			
			if(Global.level < Assets.LEVELS.length) { Global.level ++; }
			Global.finished = false;
			
			loadlevel();
		}
		
		public function gotoLevel(index:int ):void
		{
			removeAll();
			
			Global.finished = false;
			
			loadspecificlevel(index);
		}
		
		
		/**
		 * Restarts the current level from a checkpoint.
		 */
		public function restartFromCheckpoint():void
		{
			Global.player.healthCurrent = Global.player.healthMax;
			Global.player.x = Global.lastCheckpoint[0];
			Global.player.y = Global.lastCheckpoint[1];
			Global.player.dead = false;
			Global.restartFromCheckpoint = false;
			Global.deaths ++;
			
		}
		 
		/**
		 * Reloads the current level
		 * @return	void
		 */
		public function restartlevel():void
		{
			removeAll();
			loadlevel();
			
			//increase deaths
			Global.deaths ++;
		}
		
	}

}