package source 
{
	import flash.utils.Dictionary;
	import net.flashpunk.utils.Key;
	import source.Objects.LifeBar;
	import source.Objects.Player;
	import source.Control.View;
	/*
	 * This class contains a number of global variables to be used throughout the game
	 */
	public class Global
	{
		public static var
			time:int = 0,
			deaths:int = 0,
			gems:int = 0,
			level:int = 0,
			
			newgame:Boolean = false,
			loadgame:Boolean = false,
			
			musicon:Boolean = true,
			soundon:Boolean = true,
			
			keyUp:int = Key.UP,
			keyDown:int = Key.DOWN,
			keyLeft:int = Key.LEFT,
			keyRight:int = Key.RIGHT,
			keyA:int = Key.X,
			keyB:int = Key.Z,
			
			player:Player,
			view:View,
			nextLevelIndex:int,
			lastLevelIndex:int,
			Lifebar:LifeBar,
			lastCheckpoint:Array,
			switchDict:Dictionary = new Dictionary(),
			
			paused:Boolean = false,
			restart:Boolean = false,
			restartFromCheckpoint = false,
			finished:Boolean = false,
			
			hasGun:Boolean = false,
			hasDoubleJump:Boolean = false,
			hasSecurityCodes:Boolean = false,
			hasBombs:Boolean = false;
			
			
		public static const grid:int = 32;
		
	}

}