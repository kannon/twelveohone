package source 
{
	/**
	 * ...
	 * @author Noel Berry
	 */
	public class Assets
	{
		
		//Embed the levels
		[Embed(source = '../assets/levels/Test.oel',  mimeType = "application/octet-stream")] public static const TEST: Class;
		[Embed(source = '../assets/levels/Bunker.oel',  mimeType = "application/octet-stream")] public static const BUNKER: Class;
		[Embed(source = '../assets/levels/Overworld.oel',  mimeType = "application/octet-stream")] public static const OVERWORLD: Class;
		[Embed(source = '../assets/levels/PoliceStation.oel',  mimeType = "application/octet-stream")] public static const POLICESTATION: Class;
		[Embed(source = '../assets/levels/Offices.oel',  mimeType = "application/octet-stream")] public static const OFFICES: Class;

		public static const LEVELS:Array = new Array(BUNKER, OVERWORLD, POLICESTATION, OFFICES);
		
		//tilesets
		[Embed(source = '../assets/graphics/tileset.png')] public static const TILESET:Class;
		
		//projectiles
		[Embed(source = '../assets/graphics/shotright.png')] public static const PROJECTILE_BULLET_RIGHT:Class;
		[Embed(source = '../assets/graphics/shotleft.png')] public static const PROJECTILE_BULLET_LEFT:Class;
		
		//menu
		[Embed(source = '../assets/graphics/banner.png')] public static const MENU_BANNER:Class;
		[Embed(source = '../assets/graphics/menu.png')] public static const MENU:Class;
		
		//slopes
		[Embed(source = '../assets/graphics/slope1.png')] public static const SLOPE1:Class;
		[Embed(source = '../assets/graphics/slope2.png')] public static const SLOPE2:Class;
		[Embed(source = '../assets/graphics/slope3.png')] public static const SLOPE3:Class;
		[Embed(source = '../assets/graphics/slope4.png')] public static const SLOPE4:Class;
		
		//objects
		[Embed(source = '../assets/graphics/crate.png')] public static const OBJECT_CRATE:Class;
		[Embed(source = '../assets/graphics/door.png')] public static const OBJECT_DOOR:Class;
		[Embed(source = '../assets/graphics/electricity.png')] public static const OBJECT_ELECTRICITY:Class;
		[Embed(source = '../assets/graphics/newplatform.png')] public static const OBJECT_MOVING:Class;
		[Embed(source = '../assets/graphics/movingvertical.png')] public static const OBJECT_MOVINGVERTICAL:Class;
		[Embed(source = '../assets/graphics/checkpoint.png')] public static const OBJECT_CHECKPOINT:Class;
		[Embed(source = '../assets/graphics/switch.png')] public static const OBJECT_SWITCH:Class;
		
		//powerups
		[Embed(source = '../assets/graphics/gunpowerup.png')] public static const POWERUP_GUN:Class;
		[Embed(source = '../assets/graphics/jumppowerup.png')] public static const POWERUP_JUMP:Class;
		[Embed(source = '../assets/graphics/codepowerup.png')] public static const POWERUP_CODE:Class;
		
		
		
		//Hazards
		[Embed(source = '../assets/graphics/spikes.png')] public static const OBJECT_SPIKE:Class;
		[Embed(source = '../assets/graphics/slime.png')] public static const OBJECT_SLIME:Class;
		[Embed(source = '../assets/graphics/fallzone.png')] public static const OBJECT_FALLZONE:Class;
		[Embed(source = '../assets/graphics/radiation.png')] public static const OBJECT_RADIATIONLEAK:Class;
		
		
		//player
		[Embed(source = '../assets/graphics/player.png')] public static const PLAYER:Class;
		
	}

}